﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CombinationOption  {
	public float weight {get; private set;}
	public string optionName {get; private set;}
	public Color optionColor {get; private set;}
	public CombinationOption() {
		weight = 0;
		optionName = "white";
		optionColor = Color.white;
	}

	public CombinationOption(float w, string name, Color color) {
		weight = w;
		optionName = name;
		optionColor = color;
	}
}
