﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CombinationButton : MonoBehaviour {

	public ColorPicker colorPicker;
	private GameController gameController;

	private Button button;

	void Start() {
		button = this.gameObject.GetComponent<Button>();
		gameController = GameObject.FindObjectOfType<GameController>();
		Reset();
	}

	public void Reset() {
		SetColorBlockToColor(Color.white);
	}

	public void OpenPicker() {
		colorPicker.Show (button);
	}

	public Color GetColor() {
		return button.colors.normalColor;
	}

	public void SetColor(Color selectedColor) {
		SetColorBlockToColor (selectedColor);
	}

	public void SetColorBlockToColor(Color normalColor) {
		ColorBlock cb = button.colors;
		cb.normalColor = normalColor;
		Color highlightedColor = cb.normalColor;
		highlightedColor.a = 1.0f;
		Color pressedColor = cb.normalColor;
		pressedColor.a = 1.0f;
		cb.highlightedColor = highlightedColor;
		cb.pressedColor = pressedColor;
		button.colors = cb;
	}

}


