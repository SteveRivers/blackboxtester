using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CombinationLock : MonoBehaviour
{
	public CombinationButton[] LockButtons;

	private CombinationOption[] UnlockCombination;
	private bool unlocked = false;

	public void GetCombinationOptions ()
	{

	}

	public void SetLockCombination (CombinationOption[] LockCombination)
	{
		UnlockCombination = LockCombination;
	}

	public bool CheckCombination ()
	{
		bool tempUnlocked = true;
		if (LockButtons.Length < 0 || LockButtons.Length > 5) {
			unlocked = false;
			return unlocked;
		}
		for (int i = 0; i < LockButtons.Length; ++i) {
			if (!colorsAreEqual(UnlockCombination[i].optionColor, LockButtons[i].GetColor())) {
				tempUnlocked = false;
			}
		}
		unlocked = tempUnlocked;
		return unlocked;
	}

	public bool IsUnlocked ()
	{
		return unlocked;
	}

	bool colorsAreEqual(Color expected, Color actual) {
		var rOk = (Mathf.Abs(expected.r - actual.r) <= 0.001f);
		var gOk = (Mathf.Abs(expected.g - actual.g) <= 0.001f);
		var bOk = (Mathf.Abs(expected.b - actual.b) <= 0.001f);
		var aOk = (Mathf.Abs(expected.a - actual.a) <= 0.001f);
		return rOk && gOk && bOk && aOk;
	}
}