﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameController : MonoBehaviour {

	public List<CombinationOption> Colors;

	public Button ActiveButton {get; private set;}
	public Button QuitButton;
	public ColorPicker colorPicker;

	private CombinationLock combinationLock;
	private GameObject scoreText;
	private GameObject lockIndicator;

	private bool[] UnLocked;

	// Use this for initialization
	void Start () {
		Colors = SetColors ();
		scoreText = GameObject.FindGameObjectWithTag("Score");
		lockIndicator = GameObject.FindGameObjectWithTag("Status");
		combinationLock = GameObject.FindObjectOfType<CombinationLock> ();
		combinationLock.SetLockCombination (LockCombination ());
		if(Screen.fullScreen) {QuitButton.enabled = true;}
	}

	public void QuitRequested(){
		Application.Quit();
	}

	public void ResetLock() {
		colorPicker.Hide ();
		for(int i = 0; i < combinationLock.LockButtons.Length; ++i) {
			var c = new Color(1.0f, 1.0f,1.0f,1.0f);
			combinationLock.LockButtons[i].SetColorBlockToColor(c);
		}
		lockIndicator.GetComponentInChildren<Image>().color = new Color(1.0f, 0f, 0f, 1.0f);
		ActiveButton = null;
		scoreText.GetComponentInChildren<Text>().text = "-------------";
	}

	public void CheckLock() {
		colorPicker.Hide ();
		bool unlocked = false;
		var score = scoreGuess();
		scoreText.GetComponentInChildren<Text>().text = score.ToString();
		unlocked = combinationLock.CheckCombination();
		if (unlocked) {lockIndicator.GetComponentInChildren<Image>().color = new Color(0f,1.0f, 0f, 1.0f);}
	}



	void printColor(Color c) {
		Debug.Log (c.r + ":" + c.g + ":" + c.b + ":" + c.a);
	}



	float scoreGuess() {
		float score = 0f;

		for (int i = 0; i< combinationLock.LockButtons.Length; ++i) {
			var guessColor = combinationLock.LockButtons[i].GetColor();
			foreach(var thing in Colors) {
				if (thing.optionColor.Equals(guessColor)) {
					float modifier = (thing.weight > 0f) ? 72f : 60f;
					score += (modifier * thing.weight);
				}
			}
		}
		return score;
	}

	CombinationOption[] LockCombination(int tumblers = 5) {
		CombinationOption[] LockSetting = new CombinationOption[tumblers];
		LockSetting[0] = Colors.Where(x => x.optionName == "Red").SingleOrDefault();
		LockSetting[1] = Colors.Where(x => x.optionName == "Red").SingleOrDefault();
		LockSetting[2] = Colors.Where(x => x.optionName == "Green").SingleOrDefault();
		LockSetting[3] = Colors.Where(x => x.optionName == "Yellow").SingleOrDefault();
		LockSetting[4] = Colors.Where(x => x.optionName == "Violet").SingleOrDefault();
		UnLocked = new bool[tumblers];
		for(int i = 0; i < 5; ++i) {UnLocked[i] = false;}
		return LockSetting;
	}
		
		List<CombinationOption> SetColors() {
			var list = new List<CombinationOption>();
			list.Add(new CombinationOption(-8.0f, "Green", new Color(0.000f, 1.000f, 0.000f, 1.000f)));
			list.Add(new CombinationOption(4.0f, "Blue", new Color(0.000f, 0.000f, 1.000f, 1.000f)));
			list.Add(new CombinationOption(0f, "Black", new Color(0.000f, 0.000f, 0.000f, 1.000f)));
			list.Add(new CombinationOption(0f, "DarkBlue", new Color(0.137f, 0.000f, 0.702f, 1.000f)));
			list.Add(new CombinationOption(0f, "LightGrey", new Color(0.699f, 0.699f, 0.699f, 1.000f)));
			list.Add(new CombinationOption(-32.0f, "Orange", new Color(1.000f, 0.502f, 0.000f, 1.000f)));
			list.Add(new CombinationOption(0f, "DarkYellow", new Color(1.000f, 0.766f, 0.000f, 1.000f)));
			list.Add(new CombinationOption(16.0f, "Yellow", new Color(1.000f, 1.000f, 0.000f, 1.000f)));
			list.Add(new CombinationOption(1.0f, "Violet", new Color(0.502f, 0.000f, 0.827f, 1.000f)));
			list.Add(new CombinationOption(0f, "White", new Color(1.000f, 1.000f, 1.000f, 1.000f)));
			list.Add(new CombinationOption(0f, "DarkOrange", new Color(1.000f, 0.313f, 0.110f, 1.000f)));
			list.Add(new CombinationOption(-2.0f, "Indigo", new Color(0.294f, 0.000f, 0.510f, 1.000f)));
			list.Add(new CombinationOption(0f, "DarkGrey", new Color(0.500f, 0.500f, 0.500f, 1.000f)));
			list.Add(new CombinationOption(0f, "DarkGreen", new Color(0.000f, 0.506f, 0.125f, 1.000f)));
			list.Add(new CombinationOption(0f, "LimeGreen", new Color(0.674f, 1.000f, 0.360f, 1.000f)));
			list.Add(new CombinationOption(64.0f, "Red", new Color(1.000f, 0.000f, 0.000f, 1.000f)));
		return list;
	}
}
