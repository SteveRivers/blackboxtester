﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ColorPicker : MonoBehaviour {

	public List<CombinationOption> AvailableColours {get; private set;}

	private Vector3 startPosition;
	private bool isOpen = false;
	private Button activeButton;
	private Color pickedColor;

	void Start () {
		AvailableColours = SetColors ();
		startPosition = transform.position;
		this.gameObject.SetActive (isOpen);
	}

	public void Show(Button button) {
		if (isOpen && activeButton == button) {
			Hide ();
		} else {
			activeButton = button;
			isOpen = true;
			transform.position = new Vector3 (button.transform.position.x, transform.position.y, transform.position.z);
			this.gameObject.SetActive (isOpen);
		}
	}

	public void Hide() {
		isOpen = false;
		transform.position = startPosition;
		this.gameObject.SetActive (isOpen);
	}

	public void PickColor(Button button) {
		pickedColor = button.colors.normalColor;
		activeButton.colors = SetColorBlockToColor(pickedColor);
	}

	ColorBlock SetColorBlockToColor(Color normalColor) {
		ColorBlock cb = activeButton.colors;
		cb.normalColor = normalColor;
		Color highlightedColor = cb.normalColor;
		highlightedColor.a = 1.0f;
		cb.highlightedColor = highlightedColor;
		Color pressedColor = cb.normalColor;
		pressedColor.a = 1.0f;
		cb.pressedColor = pressedColor;
		return cb;
	}

	List<CombinationOption> SetColors() {
		var list = new List<CombinationOption>();
		list.Add(new CombinationOption(-8.0f, "Green", new Color(0.000f, 1.000f, 0.000f, 1.000f)));
		list.Add(new CombinationOption(4.0f, "Blue", new Color(0.000f, 0.000f, 1.000f, 1.000f)));
		list.Add(new CombinationOption(0f, "Black", new Color(0.000f, 0.000f, 0.000f, 1.000f)));
		list.Add(new CombinationOption(0f, "DarkBlue", new Color(0.137f, 0.000f, 0.702f, 1.000f)));
		list.Add(new CombinationOption(0f, "LightGrey", new Color(0.699f, 0.699f, 0.699f, 1.000f)));
		list.Add(new CombinationOption(-32.0f, "Orange", new Color(1.000f, 0.502f, 0.000f, 1.000f)));
		list.Add(new CombinationOption(0f, "DarkYellow", new Color(1.000f, 0.766f, 0.000f, 1.000f)));
		list.Add(new CombinationOption(16.0f, "Yellow", new Color(1.000f, 1.000f, 0.000f, 1.000f)));
		list.Add(new CombinationOption(1.0f, "Violet", new Color(0.502f, 0.000f, 0.827f, 1.000f)));
		list.Add(new CombinationOption(0f, "White", new Color(1.000f, 1.000f, 1.000f, 1.000f)));
		list.Add(new CombinationOption(0f, "DarkOrange", new Color(1.000f, 0.313f, 0.110f, 1.000f)));
		list.Add(new CombinationOption(-2.0f, "Indigo", new Color(0.294f, 0.000f, 0.510f, 1.000f)));
		list.Add(new CombinationOption(0f, "DarkGrey", new Color(0.500f, 0.500f, 0.500f, 1.000f)));
		list.Add(new CombinationOption(0f, "DarkGreen", new Color(0.000f, 0.506f, 0.125f, 1.000f)));
		list.Add(new CombinationOption(0f, "LimeGreen", new Color(0.674f, 1.000f, 0.360f, 1.000f)));
		list.Add(new CombinationOption(64.0f, "Red", new Color(1.000f, 0.000f, 0.000f, 1.000f)));
		return list;
	}
}
