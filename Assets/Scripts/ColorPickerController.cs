﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorPickerController : MonoBehaviour {
	private GameController gameController;

	void Start () {
		gameController = GameObject.FindObjectOfType<GameController>();
	}

	public void PickColor(Button clicked) {
		gameController.ActiveButton.colors = clicked.colors;
	}
}
